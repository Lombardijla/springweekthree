package com.oreillyauto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringweekthreeApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringweekthreeApplication.class, args);
	}

}
