package com.oreillyauto.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.oreillyauto.domain.facilities.Facility;
import com.oreillyauto.domain.facilities.Teammember;
import com.oreillyauto.domain.interns.Intern;
import com.oreillyauto.domain.orders.Order;
import com.oreillyauto.domain.orders.OrderItem;
import com.oreillyauto.domain.projects.Planner;
import com.oreillyauto.domain.projects.Project;
import com.oreillyauto.domain.schools.School;
import com.oreillyauto.domain.schools.Student;
import com.oreillyauto.model.Message;
import com.oreillyauto.model.Oreilly;
import com.oreillyauto.service.MappingsService;

@Controller
public class MappingsController {

	@Autowired
	MappingsService mappingsService;

	@GetMapping(value = { "/mappings" })
	public String getMappings(Model model) throws Exception {
		return "redirect:/mappings/orders/unidirectional-many-to-one";
	}
	
	@GetMapping(value = { "/mappings/orders/unidirectional-many-to-one" })
	public String getUniManyToOne(Model model) throws Exception {
		List<Order> orderList = mappingsService.getOrders();
		model.addAttribute("orderList", orderList);
		
		List<OrderItem> orderItemList = mappingsService.getOrderItems();
		model.addAttribute("orderItemList", orderItemList);
		
		mappingsService.addUserTablesToTheModel(model);
		return "unidirectional-many-to-one";
	}
	
	@GetMapping(value = { "/mappings/orders/unidirectional-one-to-many" })
	public String getUniOneToMany(Model model) throws Exception {
		List<School> schoolList = mappingsService.getSchools();
		model.addAttribute("schoolList", schoolList);
		
		List<Student> studentList = mappingsService.getStudents();
		model.addAttribute("studentList", studentList);

		mappingsService.addUserTablesToTheModel(model);
		return "unidirectional-one-to-many";
	}
	
	@GetMapping(value = { "/mappings/orders/bidirectional-one-to-many" })
	public String getBiOneToMany(Model model) throws Exception {
		List<Facility> facilityList = mappingsService.getFacilities();
		model.addAttribute("facilityList", facilityList);
		
		List<Teammember> teammemberList = mappingsService.getTeammembers();
		model.addAttribute("teammemberList", teammemberList);
		
		mappingsService.addUserTablesToTheModel(model);
		return "bidirectional-one-to-many";
	}
	
	@GetMapping(value = { "/mappings/orders/bidirectional-many-to-many" })
	public String getBiManyToMany(Model model) throws Exception {
		List<Planner> plannerList = mappingsService.getPlanners();
		model.addAttribute("plannerList", plannerList);
		
		List<Project> projectList = mappingsService.getProjects();
		model.addAttribute("projectList", projectList);
		
		mappingsService.addUserTablesToTheModel(model);
		return "bidirectional-many-to-many";
	}
	
	@GetMapping(value = { "/mappings/addIntern" })
	public String getAddIntern(Model model) throws Exception {	
		List<Intern> internList = mappingsService.getInterns();
		model.addAttribute("internList", internList);
		return "addIntern";
	}
	
	@ResponseBody
	@PostMapping(value = { "/mappings/addIntern" })
	public String postAddIntern(Model model, @RequestBody Intern intern) throws Exception {
		if (intern == null || intern.getInternName() == null || intern.getInternName().equals("") ||
				intern.getIssuer() == null) {
			return new Gson().toJson(new Message(Oreilly.DANGER, "Sorry, Missing Data"));	
		}
		mappingsService.saveIntern(intern);
		Message message = new Message(Oreilly.SUCCESS, "Intern Added Successfully");
		List<Intern> internList = mappingsService.getInterns();
		message.setInternList(mappingsService.getInterns());
		//model.addAttribute("internResultList", internList);
		return new Gson().toJson(message);
	}
	
}
