package com.oreillyauto.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.oreillyauto.service.UniversityService;

@Controller
public class UniversityController {

	@Autowired
	UniversityService universityService;
	
	@ResponseBody
    @GetMapping(value = {"/university"})
    public String login() {
		universityService.testUniversityQueries();
        return "done";
    }

}
