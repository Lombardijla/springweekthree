package com.oreillyauto.dao;

import org.springframework.data.repository.CrudRepository;

import com.oreillyauto.dao.custom.OrderRepositoryCustom;
import com.oreillyauto.domain.orders.Order;

public interface OrderRepository extends CrudRepository<Order, Integer>, OrderRepositoryCustom {
    
}
