package com.oreillyauto.dao.impl;

import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import com.oreillyauto.dao.custom.CourseRepositoryCustom;
import com.oreillyauto.domain.university.Course;

@Repository
public class CourseRepositoryImpl extends QuerydslRepositorySupport implements CourseRepositoryCustom {
	
    public CourseRepositoryImpl() {
        super(Course.class);
    }

	@Override
	public void testUniversityQueries() {
		
	}
    
}

