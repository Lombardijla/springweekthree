package com.oreillyauto.domain.foo;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.oreillyauto.util.TimeDateHelper;

@Entity
@Table(name = "PUPILS")
public class Pupil implements Serializable {
	private static final long serialVersionUID = -5996834027120348470L;

	public Pupil() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "tx_id", columnDefinition = "INTEGER")
	private Integer txId;

	@Column(name = "class_id", columnDefinition = "INTEGER")
	private Integer classId;

	@Column(name = "first_name", columnDefinition = "VARCHAR(64)")
	private String firstName;

	@Column(name = "last_name", columnDefinition = "VARCHAR(64)")
	private String lastName;

	@Column(name = "enroll_date", columnDefinition = "TIMESTAMP")
	private Timestamp enrollDate;
    
    @Transient
    private String enrollDateString;
    
	public Integer getTxId() {
		return txId;
	}

	public void setTxId(Integer txId) {
		this.txId = txId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Timestamp getEnrollDate() {
		return enrollDate;
	}

	public void setEnrollDate(Timestamp enrollDate) {
		this.enrollDate = enrollDate;
	}

/*	public Clazz getClazz() {
		return clazz;
	}

	public void setClazz(Clazz clazz) {
		this.clazz = clazz;
	}*/

	public Integer getClassId() {
		return classId;
	}

	public void setClassId(Integer classId) {
		this.classId = classId;
	}

	public String getEnrollDateString() {
		return enrollDateString;
	}

	public void setEnrollDateString(String enrollDateString) {
		this.enrollDateString = enrollDateString;
		
		try {
			enrollDate = TimeDateHelper.convertOrlyDatepickerToTimestamp(enrollDateString);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public String toString() {
		String ts = null;
		
		if (enrollDate != null) {
			ts = new SimpleDateFormat("MM/dd/yyyy").format(new Date(enrollDate.getTime()));
		}
		
		return "txId=" + txId + ", firstName=" + firstName + ", lastName=" + lastName + ", enrollDate="
				+ ((ts == null) ? enrollDate : ts);
	}

}
