package com.oreillyauto.model.superhero;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Superhero {

	public final String name;
		
	@JsonCreator
	public Superhero(@JsonProperty("name") String name){
	    this.name = name;
	}
}