package com.oreillyauto.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oreillyauto.dao.CourseRepository;
import com.oreillyauto.dao.UniversityRepository;
import com.oreillyauto.service.UniversityService;

@Service("universityService")
public class UniversityServiceImpl implements UniversityService {
    
	@Autowired
    CourseRepository courseRepository;
	
	@Autowired
    UniversityRepository universityRepository;
	
	@Override
	public void testUniversityQueries() {
		universityRepository.testUniversityQueries();
	}

}
