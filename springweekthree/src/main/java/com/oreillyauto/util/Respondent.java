package com.oreillyauto.util;

import java.util.ArrayList;
import java.util.List;

public class Respondent {

    Integer id;
    List<String> answerList = new ArrayList<String>();
    String timestamp;
    
    public Integer getRespondent() {
        return id;
    }

    public void setRespondent(Integer respondent) {
        this.id = respondent;
    }

    public List<String> getAnswerList() {
        return answerList;
    }

    public void setAnswerList(List<String> answerList) {
        this.answerList = answerList;
    }

    public void addAnswer(String answer) {
        answerList.add(answer);
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
    
    
}
