package com.oreillyauto.vo;

import java.util.ArrayList;
import java.util.List;

import com.oreillyauto.domain.interns.Intern;

public class InternListVO {

	public InternListVO() {
	}
	
	private List<Intern> internList = new ArrayList<Intern>();

	public List<Intern> getInternList() {
		return internList;
	}

	public void setInternList(List<Intern> internList) {
		this.internList = internList;
	}

}
